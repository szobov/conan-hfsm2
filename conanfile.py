from conans import ConanFile


class HFSM2Conan(ConanFile):
    name = "hfsm2"
    version = "0.0.1"
    revision = "master"
    description = "Hierarchical Finite State Machine Framework with Planning "
    "and Utility Support"
    license = "MIT"
    url = "https://gitlab.com/szobov/conan-hfsm2"
    exports_sources = "include/*"
    no_copy_source = True

    def source(self):
        self.run("git clone https://github.com/andrew-gresyk/HFSM2.git")
        self.run("cd HFSM2 && git checkout %s" % self.revision)

    def package(self):
        self.copy("*.hpp", dst="include", src="HFSM2/include")
        self.copy("*.inl", dst="include", src="HFSM2/include")

    def package_id(self):
        self.info.header_only()
